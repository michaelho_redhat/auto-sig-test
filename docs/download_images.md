# Downloading images produced by the Automotive SIG

The Automotive SIG provides OSBuild manifests and instructions about how to build 
images for an aarch64 virtual machine and a Raspberry Pi 4. At this time, however, 
no downloadable images exist.

In an effort to continuously deliver working images, we are developing a process to 
automatically build and test images as we update them and as CentOS Stream changes. 
This is a work in progress. 

For more information about how to build images, see [Building images](https://sigs.centos.org/automotive/building/).
